| Aplikaĵo               | Interfaco per komandlinio | Grafika interfaco |
|------------------------|---------------------------|-------------------|
| Teksta redaktilo       | nano                      | featherpad        |
| Retumilo               | nmcli                     | network-manager   |
| Sekureca ilo           | vlock                     |                   |
| Terminala administrado | screen                    |                   |
| Rimursurveilo          | htop                      |                   |
| I2C iloj               | i2c-tools                 |                   |
| Kalkulilo              | bc                        | speedcrunch       |
| Kalendaro kaj taskoj   | calcurse                  |                   |
| Retej-Stata Sciigo     | newsboat                  |                   |
| Diskospaco-analizilo   | ncdu                      | qdirstat          |
|                        | gtypist                   |                   |
|                        | toxic                     |                   |
|                        |                           | ardour            |
|                        |                           | psi               |
|                        |                           | aegisub           |
| Grafika redaktilo      |                           | inkscape          |
|                        |                           | gimp              |
|                        |                           | darktable         |
|                        | wodim, dvd+rw-tools       |                   |
|                        | hexcurse                  |                   |
| Serĉo                  | find, ripgrep             |                   |
| Muziko                 | vorbis-tools (ogg123)     | mpv               |
|                        | rhash                     | qcalcfilehash     |
| Torento                | qbittorrent-nox           | qbittorrent       |
