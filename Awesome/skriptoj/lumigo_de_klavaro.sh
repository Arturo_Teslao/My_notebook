#!/bin/bash

value=$(cat '/sys/class/leds/tpacpi::thinklight/brightness')
if [ "$value" -eq 0 ];
  then
    echo 1 > '/sys/class/leds/tpacpi::thinklight/brightness';
    stdout="La klavaro estas iluminata."
  elif [ "$value" -eq 1 ];
    then
      echo 0 > '/sys/class/leds/tpacpi::thinklight/brightness';
      stdout="La klavaro estas maliluminata"
  else
    stdout="Eraro"
fi
echo "$stdout"
