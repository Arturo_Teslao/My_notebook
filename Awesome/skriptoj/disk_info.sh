#!/bin/sh
root_info=$(df -h /                  | awk 'NR==2 {printf "%-30s %-8s %-8s\n", $6, $3, $4}')
home_info=$(df -h /home              | awk 'NR==2 {printf "%-24s %-8s %-8s\n", $6, $3, $4}')
ssd_msata_info=$(df -h /mnt/ssd_sata | awk 'NR==2 {printf "%-18s %-8s %-8s\n", $6, $3, $4}')
stdout="$root_info\n$home_info\n$ssd_msata_info\n$usb_info"
echo "$stdout"
